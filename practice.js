// Q1. Get all latitude and longitude of all the places in the following manner.
// [{place: (lat, long)}, ...]


let res1=data.reduce((acc,cur)=>{
    let temp =  acc.push({[cur.place]:`(${cur.location.lat}, ${cur.location.lng})`});
    return acc;
},[]);
console.log(res1);

// Q2. Sort data based on temperature (Low temperature first).


let res2= data.slice(0,data.length).sort((a,b)=> (a.temperature > b.temperature) ? 1 : -1);
console.log(res2);


// Q3.Rearrange data in the following format
// [{ country: { place: { location: {lat, lng }, temperature }}}, ...]


let res3 = data.reduce((acc,cur)=>{
   let {place, country, location,temperature} = cur;
   let temp = acc.push( { [country] : { [place] : { 'location' : `{${location.lat}, ${location.lng}}` }} } );
   return acc;
},[])
console.log(res3);


// Q4. Change temperature of SouthAfrica "Pretoria" to "49 Degree Celsius".

let res4=data.map((x)=>{
  if(x.place==='Pretoria' && x.country ==='SouthAfrica' ){
      let {place,country,location,temperature} = x;
      return {place,country,location,temperature:'49 Degree Celsius'};
  }
  else return x;
})
console.log(res4);


// Q5. Add a new Object in the fourth postiion.

let a={ 
      place: "Bangalore", 
      country: "India", 
      location: {
          lat: '84',
          lng: '47'
      },
      temperature: '29 Degree Celsius'
}


let res5 = data.reduce((acc,cur)=>{
      if(data.indexOf(cur)=== 3){
          acc.push(a);
      }
      acc.push(cur);
      return acc;
},[]);
console.log(res5);


//  Q6. Delete the third element in the array .

let res6 = data.reduce((acc,cur)=>{
  if(data.indexOf(cur) !== 2){
      acc.push(cur);
  }
  return acc;
},[]);
console.log(res6);


//  Q7. Swap elements at position 2 and second last.

let res7 = data.reduce((acc,cur)=>{
  let ind = data.indexOf(cur);
  if(ind === 1){
      acc[data.length -2]= cur;
  }else if(ind === data.length -2){
      acc[1] = cur;
  }
  else{
      acc[ind] = cur;
  }
  return acc;
},[...Array(data.length).keys()]);                  // initial acc=  [0,1,2..data.length]
console.log(res7);